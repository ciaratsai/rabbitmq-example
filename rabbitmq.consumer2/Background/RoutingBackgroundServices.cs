using Microsoft.Extensions.Hosting;
using rabbitmq.consumer.MQ;
using System.Threading;
using System.Threading.Tasks;

namespace rabbitmq.consumer.Background
{
    public class RoutingBackgroundServices : BackgroundService
    {
        private readonly RoutingUtility _RoutingUtility;

        public RoutingBackgroundServices(RoutingUtility routingUtility)
        {
            _RoutingUtility = routingUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _RoutingUtility.Receive();

            await Task.Delay(100);
        }
    }
}