using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using rabbitmq.consumer.Background;
using rabbitmq.consumer.MQ;
using RabbitMQ.Client;

namespace rabbitmq.consumer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            //services.AddSingleton<WorkQueueUtility>();
            //services.AddSingleton<PubSubUtility>();
            // services.AddSingleton<RoutingUtility>();
            services.AddSingleton<TopicsUtility>();

            services.AddSingleton(new ConnectionFactory()
            {
                HostName = "192.168.99.100",
                Port = 5672,
                UserName = "guest",
                Password = "guest"
            });

            //services.AddHostedService<WorkQueuesBackgroundServices>();
            //services.AddHostedService<PubSubBackgroundServices>();
            // services.AddHostedService<RoutingBackgroundServices>();
            services.AddHostedService<TopicsBackgroundServices>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
