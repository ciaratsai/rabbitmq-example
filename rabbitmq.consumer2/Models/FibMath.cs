namespace rabbitmq.consumer2.Models
{
    public class FibMath
    {
        public static int fib(int n)
        {
            if (n == 0 || n == 1)
            {
                return n;
            }

            return fib(n - 1) + fib(n - 2);
        }
    }
}