﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rabbitmq.consumer.Models
{
    public class User
    {
        public long UserID { get; set; }

        public string UserName { get; set; }
    }
}
