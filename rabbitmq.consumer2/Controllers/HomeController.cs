using Microsoft.AspNetCore.Mvc;

namespace rabbitmq.consumer.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [Route("Index")]
        [HttpGet]
        public string Index()
        {
            return "hello rabbitmq.consumer";
        }
    }
}