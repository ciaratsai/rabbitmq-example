using System;
using System.Text;
using rabbitmq.consumer.Models;
using rabbitmq.consumer2.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace rabbitmq.consumer.MQ
{
    public class RPCUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public RPCUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public void Receive()
        {
            var queueName = "rpc_queue";

            _Channel.QueueDeclare(queue: queueName, 
                                  durable: false,
                                  exclusive: false, 
                                  autoDelete: false, 
                                  arguments: null);

            _Channel.BasicQos(0, 1, false);

            var consumer = new EventingBasicConsumer(_Channel);
            _Channel.BasicConsume(queue: queueName,
                                  autoAck: false, 
                                  consumer: consumer);

            System.Diagnostics.Debug.WriteLine(" [x] Awaiting RPC requests");

            consumer.Received += (model, ea) =>
            {
                string response = null;

                var body = ea.Body.ToArray();
                var props = ea.BasicProperties;
                var replyProps = _Channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;

                try
                {
                    var message = Encoding.UTF8.GetString(body);
                    int n = int.Parse(message);

                    System.Diagnostics.Debug.WriteLine(" [.] {0} fib({1})", DateTime.Now.ToString("HH:mm:ss.fff"), message);

                    response = FibMath.fib(n).ToString();
                }
                catch (Exception e)
                {
                    Console.WriteLine(" [.] " + e.Message);
                    response = "";
                }
                finally
                {
                    var responseBytes = Encoding.UTF8.GetBytes(response);
                    _Channel.BasicPublish(exchange: "", 
                                          routingKey: props.ReplyTo,
                                          basicProperties: replyProps, 
                                          body: responseBytes);

                    _Channel.BasicAck(deliveryTag: ea.DeliveryTag,
                                      multiple: false);
                }
            };
        }
    }
}