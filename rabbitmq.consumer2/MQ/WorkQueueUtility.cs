﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;

namespace rabbitmq.consumer.MQ
{
    public class WorkQueueUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public WorkQueueUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public void Receive()
        {
            _Channel.QueueDeclare(queue: "task_queue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            _Channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            System.Diagnostics.Debug.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(_Channel);
            consumer.Received += (sender, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                System.Diagnostics.Debug.WriteLine(" [x] Received {0}", message);

                int dots = message.Split('.').Length - 1;
                Thread.Sleep(dots * 1000);

                System.Diagnostics.Debug.WriteLine(" [x] Done");

                // Note: it is possible to access the channel via
                //       ((EventingBasicConsumer)sender).Model here
                _Channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            _Channel.BasicConsume(queue: "task_queue",
                                 autoAck: false,
                                 consumer: consumer);
        }
    }
}
