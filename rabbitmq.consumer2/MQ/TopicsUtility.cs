using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace rabbitmq.consumer.MQ
{
    public class TopicsUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public TopicsUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public void Receive()
        {
            _Channel.ExchangeDeclare(exchange: "topic_logs", type: ExchangeType.Topic);
            // var queueName = _Channel.QueueDeclare().QueueName;

            var queueName = "kickout_queue2";
            _Channel.QueueDeclare(queue: queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var bindingKeys = new string[2]
            {
                "gameserver.kickout.*",
                "lobby.kickout.Vivi200"
            };

            foreach (var bindingKey in bindingKeys)
            {
                _Channel.QueueBind(queue: queueName,
                                exchange: "topic_logs",
                                routingKey: bindingKey);
            }
           
            System.Diagnostics.Debug.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(_Channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var routingKey = ea.RoutingKey;
                
                System.Diagnostics.Debug.WriteLine(" [x] {0} Received '{1}':'{2}'",
                                  DateTime.Now.ToString("HH:mm:ss.fff"), routingKey, message);
            };

            _Channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}