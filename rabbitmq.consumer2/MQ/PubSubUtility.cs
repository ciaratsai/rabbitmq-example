using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace rabbitmq.consumer.MQ
{
    public class PubSubUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public PubSubUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public void Receive()
        {
            // must be declare exchange
            _Channel.ExchangeDeclare(exchange: "logs", type: ExchangeType.Fanout);

            var queueName = _Channel.QueueDeclare().QueueName;
            _Channel.QueueBind(queue: queueName,
                              exchange: "logs",
                              routingKey: "");

            System.Diagnostics.Debug.WriteLine(" [*] Waiting for logs.");

            var consumer = new EventingBasicConsumer(_Channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                System.Diagnostics.Debug.WriteLine(" [v] {0} - {1}", DateTime.Now.ToString("HH:mm:ss.fff"), message);
            };

            _Channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}