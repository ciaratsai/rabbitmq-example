using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace rabbitmq.consumer.MQ
{
    public class RoutingUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public RoutingUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public void Receive()
        {
            // must be declare exchange
            _Channel.ExchangeDeclare(exchange: "direct_logs", type: ExchangeType.Direct);
            var queueName = _Channel.QueueDeclare().QueueName;

            var routingKeys = new string[1]
            {
                "error",
            };

            foreach (var severity in routingKeys)
            {
                _Channel.QueueBind(queue: queueName,
                                exchange: "direct_logs",
                                routingKey: severity);
            }
           
            System.Diagnostics.Debug.WriteLine(" [*] Waiting for logs.");

            var consumer = new EventingBasicConsumer(_Channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var routingKey = ea.RoutingKey;
                System.Diagnostics.Debug.WriteLine(" [x] {0} Received '{1}':'{2}'",
                                  DateTime.Now.ToString("HH:mm:ss.fff"), routingKey, message);
            };

            _Channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}