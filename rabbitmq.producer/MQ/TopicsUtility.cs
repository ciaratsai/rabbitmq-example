using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace rabbitmq.producer.MQ
{
    public class TopicsUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public TopicsUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public async Task Send<T>(string routingKey, T data)
        {
            _Channel.ExchangeDeclare(exchange: "topic_logs", type: ExchangeType.Topic);
            
            var properties = _Channel.CreateBasicProperties();
            properties.Persistent = true;

            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));
            await Task.Run(() => 
            {
                _Channel.BasicPublish(exchange: "topic_logs",
                    routingKey: routingKey,
                    basicProperties: properties,
                    body: body);
            });
        }
    }
}