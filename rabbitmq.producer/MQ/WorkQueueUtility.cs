using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace rabbitmq.producer.MQ
{
    public class WorkQueueUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public WorkQueueUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        public async Task Send<T>(T data)
        {
            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));

            var properties = _Channel.CreateBasicProperties();
            properties.Persistent = true;

            await Task.Run(() =>
            {
                _Channel.BasicPublish(exchange: "",
                                    routingKey: "task_queue",
                                    basicProperties: properties,
                                    body: body);
            });
        }
    }
}