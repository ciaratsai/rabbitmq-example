using RabbitMQ.Client;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace rabbitmq.producer.MQ
{
    public class RoutingUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public RoutingUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task Send<T>(string logType, T data)
        {
            _Channel.ExchangeDeclare(exchange: "UserExchange", type: ExchangeType.Direct);

            var properties = _Channel.CreateBasicProperties();
            properties.Persistent = true;

            var severity = logType;

            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));
            await Task.Run(() =>
            {
                _Channel.BasicPublish(exchange: "UserExchange",
                                 routingKey: severity,
                                 basicProperties: properties,
                                 body: body);

                System.Diagnostics.Debug.WriteLine(" [x] Sent '{0}':'{1}'", severity, data);
            });

        }
    }
}