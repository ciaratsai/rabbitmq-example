using System;
using System.Collections.Concurrent;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace rabbitmq.producer.MQ
{
    public class RPCUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;
        private readonly IBasicProperties _Props;
        private readonly BlockingCollection<string> _RespQueue = new BlockingCollection<string>();
        private readonly EventingBasicConsumer _Consumer;
        private string _ReplyQueueName = string.Empty;

        public RPCUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
            _Consumer = new EventingBasicConsumer(_Channel);
            
            _ReplyQueueName = _Channel.QueueDeclare().QueueName;
            _Props = _Channel.CreateBasicProperties();
            var correlationId = Guid.NewGuid().ToString();
            _Props.CorrelationId = correlationId;
            _Props.ReplyTo = _ReplyQueueName;

            _Consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var response = Encoding.UTF8.GetString(body);
                if (ea.BasicProperties.CorrelationId == correlationId)
                {
                    _RespQueue.Add(response);
                }
            };
        }

        public async Task Send<T>(T data)
        {
            var messageBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));
            _Channel.BasicPublish(
                exchange: "",
                routingKey: "rpc_queue",
                basicProperties: _Props,
                body: messageBytes);

            _Channel.BasicConsume(
                consumer: _Consumer,
                queue: _ReplyQueueName,
                autoAck: true);
        }
    }
}