using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace rabbitmq.producer.MQ
{
    public class PubSubUtility
    {
        private readonly ConnectionFactory _Factory;
        private readonly IConnection _Connection;
        private readonly IModel _Channel;

        public PubSubUtility(ConnectionFactory factory)
        {
            _Factory = factory;
            _Connection = _Factory.CreateConnection();
            _Channel = _Connection.CreateModel();
        }


        /// <summary>
        /// Cuz fanout is creating a temperary queue that is nameless.
        /// The exchange won't bind particular queue.
        /// When comsumer disconnect than temperary queue will be deleted.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task Send<T>(T data)
        {
            _Channel.ExchangeDeclare(exchange: "logs", type: ExchangeType.Fanout);

            var properties = _Channel.CreateBasicProperties();
            properties.Persistent = true;

            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));
            await Task.Run(() =>
            {
                _Channel.BasicPublish(exchange: "logs",
                                    // because nameless queue, so routingKye won't be filled in.
                                    routingKey: "",
                                    basicProperties: properties,
                                    body: body);
            });
            
        }

    }
}