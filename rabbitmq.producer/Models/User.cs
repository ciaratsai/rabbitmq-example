namespace rabbitmq.producer.Models
{
    public class User
    {
        public long UserID { get; set; }
        
        public string UserName { get; set; }
    }
}