using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.producer.Models;
using rabbitmq.producer.MQ;
using RabbitMQ.Client;

namespace rabbitmq.producer.Background
{
    public class PubSubBackgroundServices : BackgroundService
    { 
        private readonly ConnectionFactory _Factory;

        private readonly PubSubUtility _PubSubUtility;

        public PubSubBackgroundServices(ConnectionFactory factory, PubSubUtility pubSubUtility)
        {
            _Factory = factory;
            _PubSubUtility = pubSubUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int i = 1;
            while(stoppingToken.IsCancellationRequested == false)
            {
                List<User> userList = new List<User>()
                {
                    new User(){ UserID = i, UserName = "Ciara" + i.ToString() },
                };
                await _PubSubUtility.Send<List<User>>(userList);
                
                i++;

                await Task.Delay(100);
            }
        }
    }
}