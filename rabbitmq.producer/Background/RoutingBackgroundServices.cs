using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.producer.Models;
using rabbitmq.producer.MQ;
using RabbitMQ.Client;

namespace rabbitmq.producer.Background
{
    public class RoutingBackgroundServices : BackgroundService
    { 
        private readonly ConnectionFactory _Factory;

        private readonly RoutingUtility _RoutingUtility;

        public RoutingBackgroundServices(ConnectionFactory factory, RoutingUtility routingUtility)
        {
            _Factory = factory;
            _RoutingUtility = routingUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int i = 1;
            while(stoppingToken.IsCancellationRequested == false)
            {
                // List<Log> logList1 = new List<Log>()
                // {
                //     new Log(){ Message = "Hello" },
                //     new Log(){ Message = "info" },
                // };
                // await _RoutingUtility.Send<List<Log>>("info", logList1);
                
                // List<Log> logList2 = new List<Log>()
                // {
                //     new Log(){ Message = "Hello" },
                //     new Log(){ Message = "warning" },
                // };
                // await _RoutingUtility.Send<List<Log>>("warning", logList2);

                // List<Log> logList3 = new List<Log>()
                // {
                //     new Log(){ Message = "Hello" },
                //     new Log(){ Message = "error" },
                // };
                // await _RoutingUtility.Send<List<Log>>("error", logList3);


                
                if (i % 10 == 0)
                {
                    await _RoutingUtility.Send<string>("expiredJWTEvent", "eyJhbGciOiJSUzI1NiIsInR5cCI6ImF0K2p3dCJ9.eyJuYmYiOjE2MDI1Nzk0NzQsImV4cCI6MTYwMjU4MDA3NCwiaXNzIjoiaHR0cDovL3NndC1zb2NpYWwtYXBwLWF1dGguZGVmYXVsdC5zdmMuY2x1c3Rlci5sb2NhbDo1MTAxIiwiYXVkIjoic29jaWFsQXBwQXV0aEFQSSIsImNsaWVudF9pZCI6InBhc3N3b3JkX2NsaWVudCIsInN1YiI6IjliMDZmOTVlLWE0OTktNGM5NS05Yzk1LTExYTg4NzFiZjRjOSIsImF1dGhfdGltZSI6MTYwMjU3OTQ3NCwiaWRwIjoibG9jYWwiLCJzY29wZSI6WyJvcGVuaWQiLCJwcm9maWxlIiwic29jaWFsQXBwQXV0aEFQSSIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJmb3JtcyJdfQ.nc38oC-PMoqrV5TuxiVgzBCdnlkAtaWV5zT11lWwaijURK-RpqPeLgAbDs0xANeSWmnY_HpweCJWxuOXka5ZB9hiscB9M1vqzeqYDARrTa4UCeWCh0fVgTUAZMq2hX73mcwBxWf7Ol1VKDCYi_KcFJ0gPWmt0Lhzrxh6ybEAfkT8JbptZqoSD5q2SO-dho1jojnEfBnxZvhaNVgR152FNqdkc6DYaaE--zPf1LzQ_cgRfjPL6i7CIcZEG6gi0thXWNhjmnAjAzUVqAxBYfjDwTTxjVaw9GIisnJ74ud0MTFGjyG07rxmqdDz_8TLk6-Pn_61pyXi2iMaKJZDwyyqnA");
                }
                else
                {
                    await _RoutingUtility.Send<string>("NewUserRegisteredDomainEvent", "1000000000");
                }

                i++;

                await Task.Delay(1000);
            }
        }
    }
}