using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.producer.MQ;
using RabbitMQ.Client;

namespace rabbitmq.producer.Background
{
    public class RPCBackgroundServices : BackgroundService
    {
        private readonly ConnectionFactory _Factory;

        private readonly RPCUtility _RPCUtility;

        public RPCBackgroundServices(ConnectionFactory factory, RPCUtility rPCUtility)
        {
            _Factory = factory;
            _RPCUtility = rPCUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int i = 1;
            while (stoppingToken.IsCancellationRequested == false)
            {
                await _RPCUtility.Send<string>(i.ToString());
                //i++;

                await Task.Delay(100);
            }
        }
    }
}