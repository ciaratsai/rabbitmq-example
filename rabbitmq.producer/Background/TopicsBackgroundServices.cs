using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.producer.Models;
using rabbitmq.producer.MQ;
using RabbitMQ.Client;

namespace rabbitmq.producer.Background
{
    public class TopicsBackgroundServices : BackgroundService
    {
        private readonly ConnectionFactory _Factory;

        private readonly TopicsUtility _TopicsUtility;

        public TopicsBackgroundServices(ConnectionFactory factory, TopicsUtility topicsUtility)
        {
            _Factory = factory;
            _TopicsUtility = topicsUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int i = 1;
            while(stoppingToken.IsCancellationRequested == false)
            {
                List<User> userList = new List<User>()
                {
                    new User() { UserID = i, UserName = "Vivi" + i.ToString() },
                };

                await _TopicsUtility.Send<List<User>>("gameserver.kickout.*", userList);

                await _TopicsUtility.Send<List<User>>("lobby.kickout.Vivi" + i.ToString(), userList);
                i++;

                await Task.Delay(100);
            }
        }
    }
}