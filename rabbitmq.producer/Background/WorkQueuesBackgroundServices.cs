﻿using Microsoft.Extensions.Hosting;
using rabbitmq.producer.Models;
using rabbitmq.producer.MQ;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace rabbitmq.producer.Background
{
    public class WorkQueuesBackgroundServices : BackgroundService
    {
        private readonly ConnectionFactory _Factory;

        private readonly WorkQueueUtility _WorkQueueUtility;

        public WorkQueuesBackgroundServices(ConnectionFactory factory, WorkQueueUtility workQueueUtility)
        {
            _Factory = factory;
            _WorkQueueUtility = workQueueUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            List<User> userList = new List<User>()
            {
                new User(){ UserID = 1, UserName = "Ciara1" },
                new User(){ UserID = 2, UserName = "Ciara2" },
                new User(){ UserID = 3, UserName = "Ciara3" },
                new User(){ UserID = 4, UserName = "Ciara4" },
                new User(){ UserID = 5, UserName = "Ciara5" },
                new User(){ UserID = 6, UserName = "Ciara6" },
            };

            while(stoppingToken.IsCancellationRequested == false)
            {
                await _WorkQueueUtility.Send<List<User>>(userList);
                await Task.Delay(100);
            }
        }
    }
}
