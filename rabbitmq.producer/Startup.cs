using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using rabbitmq.producer.Background;
using rabbitmq.producer.MQ;
using RabbitMQ.Client;

namespace rabbitmq.producer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            // services.AddSingleton<WorkQueueUtility>();
            // services.AddSingleton<PubSubUtility>();
            services.AddSingleton<RoutingUtility>();
            // services.AddSingleton<TopicsUtility>();
            // services.AddSingleton<RPCUtility>();

            services.AddSingleton(new ConnectionFactory()
            {
                HostName = "localhost",
                Port = 5672,
                UserName = "user",
                Password = "user"
            });

            // services.AddHostedService<WorkQueuesBackgroundServices>();
            // services.AddHostedService<PubSubBackgroundServices>();
            services.AddHostedService<RoutingBackgroundServices>();
            // services.AddHostedService<TopicsBackgroundServices>();
            // services.AddHostedService<RPCBackgroundServices>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
