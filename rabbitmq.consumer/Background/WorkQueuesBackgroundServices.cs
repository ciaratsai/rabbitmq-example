﻿using Microsoft.Extensions.Hosting;
using rabbitmq.consumer.MQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace rabbitmq.consumer.Background
{
    public class WorkQueuesBackgroundServices : BackgroundService
    {
        private readonly WorkQueueUtility _WorkQueueUtility;

        public WorkQueuesBackgroundServices(WorkQueueUtility workQueueUtility)
        {
            _WorkQueueUtility = workQueueUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _WorkQueueUtility.Receive();

            await Task.Delay(100);
        }
    }
}
