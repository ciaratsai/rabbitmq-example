using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.consumer.MQ;

namespace rabbitmq.consumer.Background
{
    public class RPCBackgroundServices : BackgroundService
    {
        private readonly RPCUtility _RPCUtility;

        public RPCBackgroundServices(RPCUtility rPCUtility)
        {
            _RPCUtility = rPCUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _RPCUtility.Receive();

            await Task.Delay(100);
        }
    }
}