using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.consumer.MQ;

namespace rabbitmq.consumer.Background
{
    public class TopicsBackgroundServices : BackgroundService
    {
        private readonly TopicsUtility _TopicsUtility;

        public TopicsBackgroundServices(TopicsUtility topicsUtility)
        {
            _TopicsUtility = topicsUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _TopicsUtility.Receive();

            await Task.Delay(100);
        }
    }
}