using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using rabbitmq.consumer.MQ;

namespace rabbitmq.consumer.Background
{
    public class PubSubBackgroundServices : BackgroundService
    {
        private readonly PubSubUtility _PubSubUtility;

        public PubSubBackgroundServices(PubSubUtility pubSubUtility)
        {
            _PubSubUtility = pubSubUtility;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _PubSubUtility.Receive();

            await Task.Delay(100);
        }
    }
}